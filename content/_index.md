## Upcoming Posts

I am in the process of developing a series of posts pertaining to automating security during the SDLC, where security checks are baked-in and reported on.

Stay tuned.

-t0sche

