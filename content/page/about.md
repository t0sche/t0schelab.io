---
title: About t0sche
subtitle: Who is this guy
date: 2019
comments: false
---

I am a security-focused technology professional with a background in security automation, cloud services, and application security. I have a Master's degree in Cybersecurity, and currently hold my eJPT certification. I am preparing for my OSCP, as well as the AWS Certified Security - Specialty. Currently, I am working as a Security Engineer, specializing in cloud security and automation. My goal is to become a Penetration Tester or Red Teamer.


