---
title: Browsing the Web Safely
subtitle: With Browser Plugins and Add-ons
date: 2019-03-14
tags: ["plugins", "browsers", "privacy"]
---

I am often asked what plugins or add-ons I use on a regular basis for browsing the web safely. Granted, there is no one tool or toolset that will allow you to browse safely, but the recommended tools I list here do allow for a smoother and more streamlined browsing experience. These tools help the end user maintain more control of their security, privacy, and how their data is used and shared. However, it is important to stay vigilant while browsing. Safe browsing habits are an important skill to pick up, as well as educating those around you. I may do a post in the future on tips for safe browsing, but for now I can share these simple tools to use.

### HTTPS Everywhere

This plugin, developed by the Electronic Frontier Foundation (EFF), can be configured to force all sites you visit to serve themselves over HTTPS by rerouting HTTP requests. Out of the box, it only forces redirects to HTTPS sites if they are available, but it can be configured to only allow HTTPS. Installing this plugin is pretty simple. Just head over to [this link](https://www.eff.org/https-everywhere) and select your browser and follow the instructions for install. Having this installed takes care of having to worry about sending data over unencrypted HTTP connections.

### uBlock Origin

Ah, adblockers. Of all the adblockers on the market, uBlock Origin is my favorite. It is simple, intuitive, and just plain easy to install and use. Having this adblocker installed will take care of a majority of the ads served on the Internet. This plugin can help protect you from malverstising, which is when ads serve malware (in the form of javascript most of the time), as well as intrusive and disruptive ads. One caveat of this plugin is that certain sites won't allow you to view their content with this enabled, or they will ask you nicely to turn it off. Since ads are way to generate revenue, and if you support the site, that is up to you if you want to whitelist them. uBlock Origin is available in the Google Chrome Store and as a Firefox Add-on.

### Privacy Badger

Another plugin developed by the EFF, Privacy Badger blocks invisible trackers that are tracking your web behavior. Sites that you visit can have their own plugins installed on them to track users and build a profile or fingerprint of your browsing habits in order to serve more applicable ads or content to you. Social networks, such as Facebook, are notorious for doing this, but Privacy Badger can help stop them. Privacy Badger can be downloaded [here.](https://www.eff.org/privacybadger)

### Firefox Containers

I use Firefox as my main web browser, and I have been using this plugin since it was released as a Test Pilot add-on. This nifty plugin that allows you to separate and color code your tabs by category in order to further isolate the sharing of cookies and other data between them. This can be especially helpful when logging into the same site as two different users, or just for an extra privacy measure to prohibit cookies placed by shopping sites with your social media sites. Blocking this data sharing can further prevent these sites from building a profile or track your browsing habits. Firefox Containers can be installed [here.](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/)