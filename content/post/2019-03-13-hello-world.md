---
title: Hello World
date: 2019-03-13
---

This page is where I will be posting about my experience in Infosec, whether it be my take on security news, how-to's, write-ups, or my general experience in the field. This page does not reflect the views of my current, former, or future employers.

Happy Hacking

-t0sche
