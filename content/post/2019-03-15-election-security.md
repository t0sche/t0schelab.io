---
title: Election Security and Blockchain
subtitle: A Shower Thought
date: 2019-03-15
tags: ["electionsecurity", "cryptography", "blockchain"]
---

Yesterday, Motherboard published an [article][1] by Kim Zetter revealing that the Defense Advance Research Projects Agency (DARPA) is investing $10 million in developing a secure voting solution to prevent hacking and voting machine tampering. To summarize the article, DARPA is joining with [Galois][6] to develop open-source voting software that will be deployed on secure open-source hardware. There will be two designs, one where voters make selections on a touch screen and are given a human readable paper ballot to submit to a second machine that will scan and tally their vote. The other design is just the scanner portion of the first, where the voter fills out the ballot by hand and allows the scanner to tally their vote. Both of these designs will print a paper receipt with a cryptographic signature of their vote, which voters can verify it was tallied at a later date when all cryptographic signatures of votes are posted publically online.

To quote the article, "The optical-scan system will print a receipt with a cryptographic representation of the voter’s choices. After the election, the cryptographic values for all ballots will be published on a web site, where voters can verify that their ballot and votes are among them."

I read this part of the article as I was heading to the gym, and once I got back and got into the shower, I thought to myself, "Hey, that verificaiton system sounds a lot like blockchain." So I started doing some research on a) if blockchain had been thought of or used in the past to prevent vote tampering (hint: it has), and b) if this DARPA project intended to actually use blockchain for the verification system.

It looks like there has been a lot of buzz surrounding whether or not the blockchain would be beneficial or hurtful to the security of the voting system. In Estonia, [Nasdaq provided citizens the opportunity][2] to vote online during shareholder meetings, with those votes being tallied via the blockchain. [Voatz][3], a company who piloted an online voting system for the 2018 West Virginia Primary Elections, [refused to be transparent][4] when it came to the cryptographic solution for vote tallying, leaving the public to blindly trust the results when publicizing the cryptographic results could have restored confidence.

So, it looks like there is some controversy when implementing blockchain or other cryptographic solutions for voting integrity. Granted, the DARPA project does not aim to make voting easier with an online solution. Their aim is to make voting more secure, thus restoring faith in the American election system. 

I was unable to confirm if DARPA intended to use blockchain technology to implement the cryptographic representation of votes. I could only find [similar speculation][9] based on the wording of the article and what has been disclosed thus far. We will have to wait and see when they reveal part of this system at Def Con's Voting Village later this year.

I come back to that quote from Kim's article, and I can't help but think that they will be using a form of blockchain to cryptographically verify votes. In fact, I stumbled upon [this research paper][5] from MIT that outlines a Public Bulletin Board (PBB) system that closely correlates to the system that DARPA is proposing for public verification of votes. The authors propose that "Blockchains can be used to maintain a PBB by incentivizing miners to mine blocks consisting of a list of valid ballots, just like blocks consisting of lists of valid transactions are mined in Bitcoin." 

Imagine if you were given a receipt from a voting machine after voting, and on that receipt is a cryptographic hash of your vote. You now have tangible proof that your vote was tallied, because it can be found in the blockchain ledger posted publically online after that election ends. In my mind, implementing blockchain as the cryptographic solution for publishing vote tallies makes the most sense. 

Using a blockchain as a public ledger of votes tallied would restore a lot of confidence in American elections. Due to the recent rhetoric of voter fraud accusations, culminating in [actual voter fraud][7] in North Carolina's 9th District, a solution is direly needed to restore faith in American Democracy. I think implementing a blockchain solution to verify vote tallies is a start, and I patiently await the outcome of DARPA's push to implement a fully secure voting solution. DARPA has stepped into the vaccuum created by the doubt surrounding electronic voting systems, and has instantly become the leader.

I would love to hear your thoughts on this. Hit me up on Twitter [@t0sche][8] if you have any feedback or want to discuss further.


[1]: https://motherboard.vice.com/en_us/article/yw84q7/darpa-is-building-a-dollar10-million-open-source-secure-voting-system
[2]: https://www.zdnet.com/article/why-ripples-from-this-estonian-blockchain-experiment-may-be-felt-around-the-world
[3]: https://voatz.com
[4]: https://gcn.com/Articles/2018/10/18/blockchain-voting-less-secure.aspx?Page=2
[5]: https://courses.csail.mit.edu/6.857/2016/files/2.pdf
[6]: https://galois.com
[7]: https://ballotpedia.org/North_Carolina%27s_9th_Congressional_District_election,_2018
[8]: https://twitter.com/t0sche
[9]: https://news.slashdot.org/story/19/03/14/181201/darpa-is-building-a-10-million-open-source-secure-voting-system